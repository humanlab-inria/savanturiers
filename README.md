# Savanturiers

**Kit avec les Adafruit Feather nrf52840 Express**

Détection de proximité. Ce kit dispose d'un noeud Adafruit fixe qui détecte la proximité par Ble d'un noeud mobile et donne un avertissement vocal. Le noeud mobile détecte également des obstacles par télémétrie laser et donne un avertissement par vibration.

## Matériel

### Schémas

![image](figs/node_base.png)

![image](figs/node_mobile.png)

### Liste du matériel

* 2 [Noeuds Feather nrf52840 Express](https://learn.adafruit.com/introducing-the-adafruit-nrf52840-feather?view=all)
* 1 [Grove Recorder V3.0](https://wiki.seeedstudio.com/Grove-Recorder_v3.0/)
* 3 [Cartes télémétrie laser VL53L0X](https://learn.adafruit.com/adafruit-vl53l0x-micro-lidar-distance-sensor-breakout)
* 1 [carte Grove-Vibration_Motor/](https://wiki.seeedstudio.com/Grove-Vibration_Motor/)
* 2 [piles lipo 3.7v](https://fr.aliexpress.com/item/1005001311242458.html)

## Code Arduino

* `node_base`: code pour le noeud de base
* `node_mobile`: code pour le noeud mobile (obstacle+proximité)
* `node_recorder`: code exemple d'enregistrement et play vocal d'une phrase
* `node_obstacle`: code pour la détection d'obstacles avec 1 à 3  cartes télémétrie laser VL53L0X (défine du nombre dans le code)
* `node_proxi`: code pour la détection de proximité avec 1 noeud de base.

## Licences

* Le logiciel est sous licence [MIT](./code/COPYING)
* Les boitiers sont sous licence [Creative-Common](cao/COPYING.md)
