# Savanturiers: kit0 - Détection de proximité

## Matériel utilisé

* PC sous linux/windows [ou plus tard RasberryPi] = Base ou Gateway pour détecter le(s) tag(s)
* [Noeud Wio Lite MG126](https://wiki.seeedstudio.com/Wio-Lite-MG126) et [son wiki](https://wiki.seeedstudio.com/Wio-Lite-MG126/)

## Librairies et Logiciels nécessaires

Pour programmer le tag, il faut disposer d'un PC sous linux/windows avec l'[Arduino IDE](https://www.arduino.cc/en/software). Les instructions de programmation sont données sur le [wiki de seeedstudio](https://wiki.seeedstudio.com/Wio-Lite-MG126/)

Pour la base sous linux (testé sous Ubuntu 20.04), il faut installer:

* stack Linux Bluetooth bluez
  * `$ sudo apt-get update`
  * `$sudo apt-get install bluez`
* installer [bluepy](https://ianharvey.github.io/bluepy-doc/) sous python3 [ref](https://github.com/IanHarvey/bluepy)
  * `$ sudo apt-get install python3-pip libglib2.0-dev`
  * `$ sudo pip3 install bluepy`
* installer la librairie de synthèse vocale [gTTS](https://gtts.readthedocs.io/):
  * `$ sudo pip3 install gtts mpg123`

Pour bluepy, il peut y avoir un problème d'accés root, on peut le résoudre par:

```shell
> cd /home/pissard/.conda/envs/camin/lib/python3.7/site-packages
> sudo setcap 'cap_net_raw,cap_net_admin+eip' bluepy-helper
> ./bluepy-help 0
le on
rsp=$mgmtcode=$success
quit
```

Il existe un paquet plus haut-niveau haut-dessus de bluepy, [simpleble](https://github.com/sglvladi/simpleble) non encore utilisé. A voir...

Quelques infos sur l'[installation ble sur rasberry](https://elinux.org/RPi_Bluetooth_LE).

## Code source

* ble_tags.py = code du programme python de la base
* tags.json = fichier de configuration de ble_tags.py
* tag/tag.ino = code arduino du tag

## Utilisation

**Etape1:** Ecrire le fichier de configuration pour la base

Le fichier json de configuration `tags.json` permet d'indiquer les tags qui doivent être détectés en définissant
leur adresse mac et en y associant un nom, un seuil RSSI de détection et une phrase à énoncer lors de la détection.

Pour connaitre l'adresse mac d'un tag programmé, il suffit de l'activer et de lancer sur la base la commande blescan:

```shell
$ blescan
Scanning for devices...
    Device (new): ed:67:17:22:7b:7f (random), -44 dBm 
        Flags: <06>
        Appearance: <c003>
        Complete 16b Services: <00001812-0000-1000-8000-00805f9b34fb>
        Complete Local Name: 'Wio_BLE_Analog'
```

L'adresse mac est ici `ed:67:17:22:7b:7f`

Pour le fichier de seuil on peut fixer pour une première fois un seuil médian (ex: -50 dB). Lors des premières expérimentations, on peut alors régler ce seuil car la base affiche les puissances de réception. Ce réglage dépend vraiment de l'environnement et des distances de détection désirées.

**Etape2:** Lancer l'expérimentation

* Lancer le programme sur la base `$python ble_tag.py`
* Mettre sous tension le tag. Suivant la distance du tag à la base, cette dernière va afficher:

```shell
$python3 ble_tag.py
Load 1 tags:
TAG ed:67:17:22:7b:7f
Begin scanning
Receive tag1 at -52 dB
Receive tag1 at -50 dB
Receive tag1 at -50 dB
Receive tag1 at -49 dB
Say  Bonjour tag1
```

Le répertoire `sentences/` contient les phrases à dire générées en mp3.
## TODO

* Passer à une base rasberry 0 ou 2
* Intégrer le monitoring de la batterie du tag par la base pour indiquer quand elle est déchargée
* Estimation expérimentale des durées de batterie
