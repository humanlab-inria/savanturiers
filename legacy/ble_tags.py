import json
import sys
import os
from gtts import gTTS 
from bluepy.btle import Scanner, DefaultDelegate

SCAN_TIME = 1.0

def mac_search(mac, tags):
    idx = 0
    found = -1
    for tag in tags:
        if tag["mac"] == mac:
            found = idx
            break
        else:
            idx = idx + 1

    return found


try:
    f = open('tags.json')
    tags = json.load(f)

    print("Load", len(tags), "tags:")
    register_tags=[]
    mp3_tags=[]
    idx = 0
    for tag in tags:
        register_tags.append(0)
        voice = gTTS(text=tag["sentence"], lang='fr', slow=False)
        mp3_file = "sentences/tag_"+str(idx)+".mp3"
        voice.save(mp3_file)
        mp3_tags.append(mp3_file)
        print("TAG", tag["mac"])
        idx = idx + 1



    print("Begin scanning")
    scanner = Scanner()

    while True:
        devices = scanner.scan(SCAN_TIME)

        for dev in devices:
            #print("Device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi))
            id = mac_search(dev.addr, tags)
            if  id >= 0:
                print("Receive", tags[id]["name"], "at", dev.rssi, "dB")
                if dev.rssi > tags[id]["threshold"]:   
                    if register_tags[id] == 0 :
                        print("Say ", tags[id]["sentence"])
                        os.system("mpg321 "+ mp3_tags[id]) 
                        register_tags[id] = 1
                else:
                    register_tags[id] = 0
                
except:
    print("Unexpected error:", sys.exc_info()[0])
    raise
