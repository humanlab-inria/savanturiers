#include <Arduino.h>
#include "MG126_Ble.h"
#include "SPI.h"

/** 
 * description:
 * The log info print to SerialUSB
 * The functionality implemented by this example:
 *   Using a linux PC/RasberryPy0 'gateway' to detect the tag 
 *   can monitor the level of battery tag in real time via Bluetooth.
 * 
 * Operating procedures : 
 * 1. launch the python program on the gateway: > python ble_tag.py
 *    the configuration file tags.json must be defined with:
 *    - the mac adress, its name, the detection rssi threshold and the sentence to say
 * 2. On the tag
 * 3. On the gateway, during the detection, you will:
 * ...
 * Receive tag2 at -66 dB
 * Receive tag2 at -61 dB
 * Receive tag2 at -40 dB
 * Say  Bonjour tag2
 * ...
 * 
 * */


/* Ble pins */
#define CS_PIN 47
#define IRQ_PIN 7
/* Batt pins */
#define CTRL_PIN      25
#define VOL_PIN       8

/*The ADC comparator voltage*/
#define SYS_VOL   3.3
/*The resolution of ADC*/
#define ADC_RES   1024


char analog_str[10];
float voltage = 0;

MG126_Ble_Class MG126_Ble(CS_PIN,IRQ_PIN);


void setup() {

	SERIAL.begin(115200);
  delay(100);
	SERIAL.println("\n");
	SERIAL.println("configure Ble  ...");
	MG126_Ble.ble_init();
	pinMode(CTRL_PIN,OUTPUT);
  digitalWrite(CTRL_PIN,0);
  voltage = 0;
}


void loop() 
{
  /* Send the battery value every 10 sec */
  voltage = SYS_VOL *  analogRead(VOL_PIN) * 2 / ADC_RES;

  SERIAL.print("The battery voltage is ");
  SERIAL.println(voltage);
  SERIAL.println("");
 
  sprintf(analog_str,"%.2f",voltage);
	sconn_notifydata((uint8_t*)analog_str,strlen(analog_str));
	delay(10000);
}
