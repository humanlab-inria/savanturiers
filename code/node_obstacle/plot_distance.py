#
# Copyright 2022 Inria
# 
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
#

import argparse
import pandas as pd
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Plot sensor log [filename=(output/log_distance.csv by default)]")
parser.add_argument("filename", nargs='?', help="Sensor log filename in cvs format")
args = parser.parse_args()

if args.filename == None:
    sensorlog = "output/log_distance.csv"
else:
    sensorlog = args.filename

print("OPTION ", sensorlog)

df_sensor = pd.read_csv(sensorlog)  

df_sensor.plot(y = 'distance', marker='+', grid = True, title = 'Distance')
plt.show()
