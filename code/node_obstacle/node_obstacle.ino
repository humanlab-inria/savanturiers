/*
* Copyright 2022 Inria
*  
* Use of this source code is governed by an MIT-style
* license that can be found in the LICENSE file or at
* https://opensource.org/licenses/MIT.
*
*/

#include "Adafruit_VL53L0X.h"
#include "Adafruit_NeoPixel.h"

#define NB_SENSORS 1
#define NB_MAX_SENSORS 3
#define THRESHOLD_MAX 500
#define THRESHOLD_MIN 50

// address we will assign if dual sensor is present
int LOX_ADDRESS[NB_MAX_SENSORS] = {0x30, 0x31, 0x32};

// set the pins to shutdown
int SHT_LOX[NB_MAX_SENSORS] = {5, 6, 9};

// objects for the vl53l0x
Adafruit_VL53L0X lox[NB_MAX_SENSORS] = {Adafruit_VL53L0X(), Adafruit_VL53L0X(), Adafruit_VL53L0X()};

// this holds the measurement
VL53L0X_RangingMeasurementData_t measure[NB_MAX_SENSORS];

// NeoPixel LED on the Feather nrf5284 Express board
Adafruit_NeoPixel neopixel = Adafruit_NeoPixel(1, PIN_NEOPIXEL, NEO_GRB + NEO_KHZ400);


/*
    Reset all sensors by setting all of their XSHUT pins low for delay(10), then set all XSHUT high to bring out of reset
    Keep sensor #1 awake by keeping XSHUT pin high
    Put all other sensors into shutdown by pulling XSHUT pins low
    Initialize sensor #1 with lox.begin(new_i2c_address) Pick any number but 0x29 and it must be under 0x7F. Going with 0x30 to 0x3F is probably OK.
    Keep sensor #1 awake, and now bring sensor #2 out of reset by setting its XSHUT pin high.
    Initialize sensor #2 with lox.begin(new_i2c_address) Pick any number but 0x29 and whatever you set the first sensor to
 */
void setID()
{
  // all reset
  for (int i; i < NB_SENSORS; i++)
    digitalWrite(SHT_LOX[i], LOW);
  delay(10);
  // all unreset
  for (int i; i < NB_SENSORS; i++)
    digitalWrite(SHT_LOX[i], HIGH);
  delay(10);
  // activating LOX1 and reseting LOX2

  for (int i; i < NB_SENSORS; i++)
  {
    // activating LOXi and reseting others
    Serial.print(i);
    digitalWrite(SHT_LOX[i], HIGH);
    for (int j = i + 1; j < NB_SENSORS; j++)
      digitalWrite(SHT_LOX[j], LOW);
    // initing LOXi
    if (!lox[i].begin(LOX_ADDRESS[i]))
    {
      Serial.print(F("Failed to boot VL53L0X #"));
      Serial.print(F(i));
      while (1)
        ;
    }
    delay(10);
  }
}

void read_sensors()
{
  int colors[NB_MAX_SENSORS]= {0, 0, 0};

  for (int i; i < NB_SENSORS; i++)
    lox[i].rangingTest(&measure[i], false); // pass in 'true' to get debug data printout!

  for (int i; i < NB_SENSORS; i++)
  {
    // print sensor one reading
    Serial.print(i);
    Serial.print(F(": "));
    if (measure[i].RangeStatus != 4)
    { // if not out of range
      Serial.print(measure[i].RangeMilliMeter);
      if ((measure[i].RangeMilliMeter <= THRESHOLD_MAX) && (measure[i].RangeMilliMeter >= THRESHOLD_MIN))
       colors[i] = 255;
    }
    else
    {
      Serial.print(F("Out of range"));
    }

    Serial.print(F(" "));
  }

  // Sensor0 activated -> R=255, Sensor1 activated -> G=255, Sensor2 activated -> B=255, 
  neopixel.setPixelColor(0, neopixel.Color(colors[0], colors[1], colors[2])); 
  neopixel.show(); 

  Serial.println();
}

void setup()
{
  Serial.begin(115200);

  // wait until serial port opens for native USB devices
  while (!Serial)
  {
    delay(1);
  }

  // NeoPixel init
  neopixel.begin();
  neopixel.setPixelColor(0, neopixel.Color(0, 150, 0)); // Moderately bright green color.
  neopixel.show(); 

  for (int i; i < NB_SENSORS; i++)
    pinMode(SHT_LOX[i], OUTPUT);

  Serial.println(F("Shutdown pins inited..."));

  for (int i; i < NB_SENSORS; i++)
    digitalWrite(SHT_LOX[i], LOW);

  Serial.println(F("Both in reset mode...(pins are low)"));

  Serial.println(F("Starting..."));

  setID();
}

void loop()
{

  read_sensors();
  delay(100);
}
