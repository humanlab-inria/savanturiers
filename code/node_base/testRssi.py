#
# Copyright 2022 Inria
# 
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.
#

import pandas as pd
import matplotlib.pyplot as plt
import statistics as stat
import math

TIME_DELAY = (10 * 1000)
RSSI_THRESHOLD = -50
RSSI_MARGIN = 5

df = pd.read_csv('data.csv', sep=' ', header=None, names=['rssi','rssi_mean', 'timestamp','near_first','play'])
df = df.astype(int)

print(df.head())

near_record_timestamp = 0
near_record_first = False
win_size = 10
win_rssi = [-80] * win_size

def soft_max(l_values): 
    softmax_coef = 4.342
    ll = len(l_values)
    vm = 0.0
    if  ll > 0:
        vlog = 0.0
        for val in l_values:
            vlog += math.exp(val / softmax_coef)
        vm = softmax_coef * math.log(vlog / ll)
    return vm


def proximityDetection(i, record):

    global near_record_timestamp, near_record_first

    delay = record['timestamp'] - near_record_timestamp
    #print(i, record['rssi'])
    val = 0
    win_rssi[i%win_size] = record['rssi']
    mean_rssi = stat.mean(win_rssi) 
    #mean_rssi = soft_max(win_rssi) 
    if (near_record_first == False)  and (mean_rssi  > (RSSI_THRESHOLD + RSSI_MARGIN) ) :
        #Serial.printf("%d %d %d 1", records[0].rssi, delay, near_record.first);
        print(i, record['rssi'], delay, near_record_first)
        val = 1
        near_record_first = True
        near_record_timestamp = record['timestamp']

    if mean_rssi <= (RSSI_THRESHOLD - RSSI_MARGIN) :
        near_record_first = False

    return mean_rssi, val
  
tplay = []
tmean_rssi = []
for i, record in df.iterrows():
    mean_rssi, play =  proximityDetection(i, record)
    tmean_rssi.append(mean_rssi)
    tplay.append(play)


df['newplay'] = tplay
df['rssi_mean_offline'] = tmean_rssi

ax = df.plot(kind='line', y='rssi', use_index=True)
df.plot(kind='line', y='rssi_mean', use_index=True, ax=ax)
df.plot(kind='line', y='rssi_mean_offline', use_index=True, ax=ax)
df.plot(kind='line', y='play', use_index=True, ax=ax)
df.plot(kind='line', y='newplay', use_index=True, ax=ax)
#df.plot(kind='line', y='timestamp', use_index=True, ax=ax)
plt.axhline(y=-50, color='r', linestyle='-')

plt.show()