/*
* Copyright 2022 Inria
* 
* Largely inspired by an Adafruit example for the nRF52 based Bluefruit LE modules
* https://github.com/adafruit/Adafruit_nRF52_Arduino/tree/master/libraries/Bluefruit52Lib/examples
* 
* Use of this source code is governed by an MIT-style
* license that can be found in the LICENSE file or at
* https://opensource.org/licenses/MIT.
*
*/

/*  This example constantly advertises a custom 128-bit UUID, and is
 *  intended to be used in combination with a Central sketch that scans
 *  for this UUID, and then displays an alert message, sorting matching
 *  devices by their RSSI level which is an approximate indication of
 *  distance (although highly subject to environmental obstacles).
 *
 *  By including a custom UUID in the advertising packet, we can easily
 *  filter the scan results on the Central device, rather than manually
 *  parsing the advertising packet(s) of every device in range.
 *
 *  This example is intended to be run with the *_central.ino version
 *  of this application.
 */

#include <bluefruit.h>
#include <ble_gap.h>
#include "Adafruit_VL53L0X.h"
#include "Adafruit_NeoPixel.h"

#define NB_SENSORS 1
#define NB_MAX_SENSORS 3
#define THRESHOLD_MAX 500
#define THRESHOLD_MIN 50

// Software Timer for blinking RED LED
SoftwareTimer blinkTimer;

// Custom UUID used to differentiate this device.
// Use any online UUID generator to generate a valid UUID.
// Note that the byte order is reversed ... CUSTOM_UUID
// below corresponds to the follow value:
// df67ff1a-718f-11e7-8cf7-a6006ad3dba0
const uint8_t CUSTOM_UUID[] =
    {
        0xA0, 0xDB, 0xD3, 0x6A, 0x00, 0xA6, 0xF7, 0x8C,
        0xE7, 0x11, 0x8F, 0x71, 0x1A, 0xFF, 0x67, 0xDF};

BLEUuid uuid = BLEUuid(CUSTOM_UUID);

// address we will assign if dual sensor is present
int LOX_ADDRESS[NB_MAX_SENSORS] = {0x30, 0x31, 0x32};

// set the pins to shutdown
int SHT_LOX[NB_MAX_SENSORS] = {6, 5, 9};

// objects for the vl53l0x
Adafruit_VL53L0X lox[NB_MAX_SENSORS] = {Adafruit_VL53L0X(), Adafruit_VL53L0X(), Adafruit_VL53L0X()};

// this holds the measurement
VL53L0X_RangingMeasurementData_t measure[NB_MAX_SENSORS];

// NeoPixel LED on the Feather nrf5284 Express board
Adafruit_NeoPixel neopixel = Adafruit_NeoPixel(1, PIN_NEOPIXEL, NEO_GRB + NEO_KHZ400);

// Pin on Vibration Motor
int vibreur = 10;

/*
    Reset all sensors by setting all of their XSHUT pins low for delay(10), then set all XSHUT high to bring out of reset
    Keep sensor #1 awake by keeping XSHUT pin high
    Put all other sensors into shutdown by pulling XSHUT pins low
    Initialize sensor #1 with lox.begin(new_i2c_address) Pick any number but 0x29 and it must be under 0x7F. Going with 0x30 to 0x3F is probably OK.
    Keep sensor #1 awake, and now bring sensor #2 out of reset by setting its XSHUT pin high.
    Initialize sensor #2 with lox.begin(new_i2c_address) Pick any number but 0x29 and whatever you set the first sensor to
 */
void setID()
{
  // all reset
  for (int i; i < NB_SENSORS; i++)
    digitalWrite(SHT_LOX[i], LOW);
  delay(10);
  // all unreset
  for (int i; i < NB_SENSORS; i++)
    digitalWrite(SHT_LOX[i], HIGH);
  delay(10);
  // activating LOX1 and reseting LOX2

  for (int i; i < NB_SENSORS; i++)
  {
    // activating LOXi and reseting others
    Serial.print(i);
    digitalWrite(SHT_LOX[i], HIGH);
    for (int j = i + 1; j < NB_SENSORS; j++)
      digitalWrite(SHT_LOX[j], LOW);
    // initing LOXi
    if (!lox[i].begin(LOX_ADDRESS[i]))
    {
      Serial.print(F("Failed to boot VL53L0X #"));
      Serial.print(F(i));
      while (1)
        ;
    }
    delay(10);
  }
}

void read_sensors()
{
  int colors[NB_MAX_SENSORS] = {0, 0, 0};
  int vibreur_state = LOW;

  for (int i; i < NB_SENSORS; i++)
    lox[i].rangingTest(&measure[i], false); // pass in 'true' to get debug data printout!

  for (int i; i < NB_SENSORS; i++)
  {
    // print sensor one reading
    Serial.print(i);
    Serial.print(F(": "));
    if (measure[i].RangeStatus != 4)
    { // if not out of range
      Serial.print(measure[i].RangeMilliMeter);
      if ((measure[i].RangeMilliMeter <= THRESHOLD_MAX) && (measure[i].RangeMilliMeter >= THRESHOLD_MIN))
      {
        colors[i] = 255;
        vibreur_state = HIGH;
      }
    }
    else
    {
      Serial.print(F("Out of range"));
    }

    Serial.print(F(" "));
  }

  // Sensor0 activated -> R=255, Sensor1 activated -> G=255, Sensor2 activated -> B=255,
  neopixel.setPixelColor(0, neopixel.Color(colors[0], colors[1], colors[2]));
  neopixel.show();
  digitalWrite(vibreur, vibreur_state);

  Serial.println();
}

// vibreur counter
int vibreur_counter = 0;

void read_sensors_tempo()
{
  int colors[NB_MAX_SENSORS] = {0, 0, 0};
  int vibreur_state = LOW;

  for (int i; i < NB_SENSORS; i++)
    lox[i].rangingTest(&measure[i], false); // pass in 'true' to get debug data printout!

  for (int i; i < NB_SENSORS; i++)
  {
    // print sensor one reading
    Serial.print(i);
    Serial.print(F(": "));
    if (measure[i].RangeStatus != 4)
    { // if not out of range
      Serial.print(measure[i].RangeMilliMeter);
      if ((measure[i].RangeMilliMeter <= THRESHOLD_MAX) && (measure[i].RangeMilliMeter >= THRESHOLD_MIN))
      {
        colors[i] = 255;
        vibreur_state = HIGH;
        vibreur_counter += 1;
      }
    }
    else
    {
      Serial.print(F("Out of range"));
    }

    Serial.print(F(" "));
  }

  // Sensor0 activated -> R=255, Sensor1 activated -> G=255, Sensor2 activated -> B=255,
  neopixel.setPixelColor(0, neopixel.Color(colors[0], colors[1], colors[2]));
  neopixel.show();
  if (vibreur_state == HIGH && vibreur_counter >= 5)
  {
    digitalWrite(vibreur, vibreur_state);
  }
  else
  {
    if (vibreur_state == LOW)
    {
      vibreur_counter = 0;
    }
    else
    {
      vibreur_state = LOW;
    }
    digitalWrite(vibreur, vibreur_state);
  }
  Serial.println();
}

void setup()
{
  Serial.begin(115200);
  /*
  while ( !Serial ) delay(10);   // for nrf52840 with native usb
  */

  Serial.println("Bluefruit52 Peripheral Proximity Example");
  Serial.println("----------------------------------------\n");

  // NeoPixel init
  neopixel.begin();
  neopixel.setPixelColor(0, neopixel.Color(0, 150, 0)); // Moderately bright green color.
  neopixel.show();
  digitalWrite(vibreur, LOW);
  for (int i; i < NB_SENSORS; i++)
    pinMode(SHT_LOX[i], OUTPUT);

  Serial.println(F("Shutdown pins inited..."));

  for (int i; i < NB_SENSORS; i++)
    digitalWrite(SHT_LOX[i], LOW);

  Serial.println(F("Both in reset mode...(pins are low)"));

  Serial.println(F("Starting..."));

  setID();

  // Initialize blinkTimer for 1000 ms and start it
  blinkTimer.begin(1000, blink_timer_callback);
  blinkTimer.start();

  // Set Vibration motor OUTPUT
  pinMode(vibreur, OUTPUT);

  if (!Bluefruit.begin())
  {
    Serial.println("Unable to init Bluefruit");
    while (1)
    {
      digitalToggle(LED_RED);
      delay(100);
    }
  }
  else
  {
    Serial.println("Bluefruit initialized (peripheral mode)");
  }

  Bluefruit.setTxPower(4); // Check bluefruit.h for supported values
  Bluefruit.setName("Bluefruit52");

  // Set up and start advertising
  startAdv();

  Serial.println("Advertising started");
}

void startAdv(void)
{
  // Note: The entire advertising packet is limited to 31 bytes!

  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();

  // Preferred Solution: Add a custom UUID to the advertising payload, which
  // we will look for on the Central side via Bluefruit.Scanner.filterUuid(uuid);
  // A valid 128-bit UUID can be generated online with almost no chance of conflict
  // with another device or etup
  Bluefruit.Advertising.addUuid(uuid);

  /*
  // Alternative Solution: Manufacturer Specific Data (MSD)
  // You could also send a custom MSD payload and filter for the 'Company ID'
  // via 'Bluefruit.Scanner.filterMSD(CID);', although this does require a
  // valid CID, which is why the UUID method above is more appropriate in
  // most situations. For a complete list of valid company IDs see:
  // https://www.bluetooth.com/specifications/assigned-numbers/company-identifiers
  // For test purposes, 0xFFFF CAN be used, but according to the Bluetooth SIG:
  // > "This value may be used in the internal and interoperability tests before a
  // >  Company ID has been assigned. This value shall not be used in shipping end
  // >  products."
  uint8_t msd_payload[4]; // Two bytes are required for the CID, so we have 2 bytes user data, expand as needed
  uint16_t msd_cid = 0xFFFF;
  memset(msd_payload, 0, sizeof(msd_payload));
  memcpy(msd_payload, (uint8_t*)&msd_cid, sizeof(msd_cid));
  msd_payload[2] = 0x11;
  msd_payload[3] = 0x22;
  Bluefruit.Advertising.addData(BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA, msd_payload, sizeof(msd_payload));
  */

  // Not enough room in the advertising packet for name
  // so store it in the Scan Response instead
  Bluefruit.ScanResponse.addName();

  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   *
   * For recommended advertising interval
   * https://developer.apple.com/library/content/qa/qa1931/_index.html
   */
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244); // in units of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);   // number of seconds in fast mode
  Bluefruit.Advertising.start();
}

void loop()
{
  read_sensors_tempo();
  delay(100);
}

/**
 * Software Timer callback is invoked via a built-in FreeRTOS thread with
 * minimal stack size. Therefore it should be as simple as possible. If
 * a periodically heavy task is needed, please use Scheduler.startLoop() to
 * create a dedicated task for it.
 *
 * More information http://www.freertos.org/RTOS-software-timer.html
 */
void blink_timer_callback(TimerHandle_t xTimerID)
{
  (void)xTimerID;
  digitalToggle(LED_RED);
}
